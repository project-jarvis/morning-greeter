using System;

namespace Jarvis.MorningGreeter.Speech
{
    interface ISpeechProvider {
        void SpeakMessage(string message);
    }
}
