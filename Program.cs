﻿namespace Jarvis.MorningGreeter
{
    using System;
    
    using Speech;
    using Text;

    class Program
    {
        static void Main(string[] args)
        {
            Program.SpeakGreeting(null, null);
        }

        static void SpeakGreeting(ITextProvider textProvider, ISpeechProvider speechProvider) {
            Console.WriteLine("Generating message.");
            var message = textProvider.GenerateMessage();
            Console.WriteLine($"Generated message: \"{message}\"");

            Console.WriteLine("Speaking message");
            speechProvider.SpeakMessage(message);
            Console.WriteLine("Done");
        }
    }
}
