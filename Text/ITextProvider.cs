using System;

namespace Jarvis.MorningGreeter.Text
{
    interface ITextProvider {
        string GenerateMessage();
    }
}
